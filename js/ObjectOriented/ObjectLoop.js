// 1강. Object ㅓㄴ
var memberArray = ['egoing', 'graphittie', 'leezche'];
console.group('Array Loop');
// console.log('memberArray[2]', memberArray[1])
var i = 0;
// console.log('memberArray Size : ',memberArray.length)
while(i < memberArray.length){
  console.log(i, memberArray[i]);
  i = i + 1;
}
console.groupEnd('Array Loop');

var memberObject = {
   manager:'egoing'
  ,developer:'graphittie'
  ,designer:'leezche'
}
// console.log('memberObject.designer', memberObject.designer)
// memberObject.designer = 'leezche!'
// console.log('memberObject["designer"]', memberObject['designer'])
// delete memberObject.manager
// console.log('memberObject.designer', memberObject.manager)

console.group('object loop')
for(var name in memberObject){
  // console.log(name, memberObject.name); // 변수는 .뒤에서 작용될수 없다.
  console.log(name, memberObject[name]);  // 변수는 대괄호 안에서 가능.
}
console.groupEnd('object loop')
